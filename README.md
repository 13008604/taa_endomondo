BDD :
Type : MySQL
Nom de la base : endomondo
User : 'root'
Mot de passe : '' 

Lancer le projet :
Déployer le webservice : Run as Tomcat server dans Eclipse
Lancer la BDD : WAMP (ou autre)
Lancer yeoman : grunt server

Pour initialiser la BDD :
http://adresse_du_webservice/initBDD/init
Créé un utilisateur par défaut (admin@admin.com / admin) et des types de sports par défaut

Ce qui fonctionne :
Connection
Gestion du profil
Visualisation des séances
Filter les séances
Ajout d'une séance
Détail d'une séance
Modification d'une séance

Ce qui fonctionne moins bien :
Ce qui n'est pas fait..

Auteurs :
GUILLEMIN Marion
RENOU Maxime