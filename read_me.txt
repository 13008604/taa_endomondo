BDD :
Type : MySQL
Nom de la base : endomondo
User : 'root'
Mot de passe : '' 

Lancer le projet :
D�ployer le webservice : Run as Tomcat server dans Eclipse
Lancer la BDD : WAMP (ou autre)
Lancer yeoman : grunt server

Pour initialiser la BDD :
http://adresse_du_webservice/initBDD/init
Cr�� un utilisateur par d�faut (admin@admin.com / admin) et des types de sports par d�faut

Ce qui fonctionne :
Connection
Gestion du profil
Visualisation des s�ances
Filter les s�ances
Ajout d'une s�ance
D�tail d'une s�ance
Modification d'une s�ance

Ce qui fonctionne moins bien :
Ce qui n'est pas fait..

Auteurs :
GUILLEMIN Marion
RENOU Maxime