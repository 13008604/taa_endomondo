package fr.endoMondoAPI;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import fr.endomondoEntity.Meteo;
import fr.endomondoEntity.TypeSport;
import fr.endomondoEntity.Utilisateur;

@Path("/initBDD")
public class InitBDD {

	private EntityManagerFactory emf;
	private EntityManager em;

	public InitBDD() {
		// Use persistence.xml configuration
		emf = Persistence.createEntityManagerFactory("endoMondoCreate");
		// Retrieve an entity manager
		em = emf.createEntityManager();
	}
	
	@GET
	@Path("/init")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response UserAdmin(){
		Utilisateur user = new Utilisateur();
		user.setNom("admin");
		user.setPrenom("admin");
		user.setAdresseMail("admin@admin.com");
		user.setMdp("admin");
		
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(user);
		
		
		//Initialisation des types de sport par défaut
		TypeSport ts = new TypeSport();
		ts.setNomSport("Course");
		em.persist(ts);
		
		TypeSport ts2 = new TypeSport();
		ts2.setNomSport("Marche");
		em.persist(ts2);
		
		TypeSport ts3 = new TypeSport();
		ts3.setNomSport("Velo");
		em.persist(ts3);
		
		TypeSport ts4 = new TypeSport();
		ts4.setNomSport("Raquettes");
		em.persist(ts4);
		
		//Initialisation des différentes météo par défaut
		Meteo m = new Meteo();
		m.setNomMeteo("Ensoleillé");
		em.persist(m);
		
		Meteo m2 = new Meteo();
		m2.setNomMeteo("Pluvieux");
		em.persist(m2);
		
		Meteo m3 = new Meteo();
		m3.setNomMeteo("Neigeux");
		em.persist(m3);
		
		Meteo m4 = new Meteo();
		m4.setNomMeteo("Tempête");
		em.persist(m4);

		tx.commit();
		tx = null;
		
		return Response.ok().build();
	}

}
