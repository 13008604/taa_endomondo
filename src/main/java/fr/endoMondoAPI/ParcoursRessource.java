package fr.endoMondoAPI;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import fr.endoMondoDAO.ParcoursDAO;
import fr.endomondoEntity.Parcours;


@Path("/parcours")
public class ParcoursRessource
{
	private EntityManagerFactory emf;
	private EntityManager em;

	public ParcoursRessource() {
		// Use persistence.xml configuration
		emf = Persistence.createEntityManagerFactory("endoMondoUpDate");
		// Retrieve an entity manager
		em = emf.createEntityManager();
	}

	/**
	 * Method that return a seance
	 * @param id
	 * @return
	 */
	@GET
	@Path("getParcours/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getParcours(@PathParam("id") int id) {
		ParcoursDAO parcoursDAO = new ParcoursDAO(em);
		return Response.ok(parcoursDAO.getParcoursById(id)).build();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String sayPlainTextHello() {
		return "Hello Jersey parcours !!";
	}

	/**
	 * Method that creates parcours 
	 * @param user
	 * @return
	 */
	@POST @Consumes({ MediaType.APPLICATION_JSON })
	@Path("/createParcours")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response register(Parcours parcours) {

		ParcoursDAO parcoursDAO = new ParcoursDAO(em);
	
		Boolean exec = parcoursDAO.create(parcours);

		ResponseBuilder builder;
		if(exec){
			builder = Response.ok(parcours);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/createParcours")
	public Response getOptionsCreate(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}
	
	/**
	 * Method that modifies parcours info
	 * @param parcours
	 * @return
	 */
	@POST @Consumes({ MediaType.APPLICATION_JSON })
	@Path("/updateParcours")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response UpdateParcours(Parcours parcours) {

		ParcoursDAO parcoursDAO = new ParcoursDAO(em);
		Parcours parcoursBase = parcoursDAO.getParcoursById(parcours.getId());
		
		Boolean exec = parcoursDAO.update(parcoursBase);

		ResponseBuilder builder;
		if(exec){
			builder = Response.ok(parcours);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/updateParcours")
	public Response getOptionsUpdate(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}


	/**
	 * method that deletes a parcours
	 * @param id
	 * @return
	 */
	@DELETE
	@Path("/delete/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteById(@PathParam("id") int id) {
		ParcoursDAO parcoursDAO = new ParcoursDAO(em);

		Boolean bool = parcoursDAO.delete(id);

		ResponseBuilder builder;
		if(bool){
			builder = Response.ok(id);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}


		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();

	}
	@OPTIONS
	@Path("/delete/{id}")
	public Response getOptionsDeleteUser(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

}

