package fr.endoMondoAPI;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import fr.endoMondoDAO.SeanceDAO;
import fr.endoMondoDAO.UtilisateurDAO;
import fr.endomondoEntity.Seance;
import fr.endomondoEntity.Utilisateur;


@Path("/seances")
public class SeanceRessource
{
	private EntityManagerFactory emf;
	private EntityManager em;

	public SeanceRessource() {
		// Use persistence.xml configuration
		emf = Persistence.createEntityManagerFactory("endoMondoUpDate");
		// Retrieve an entity manager
		em = emf.createEntityManager();
	}

	/**
	 * Method that return a seance
	 * @param id
	 * @return
	 */
	@GET
	@Path("getSeance/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getSeance(@PathParam("id") int id) {
		SeanceDAO seanceDAO = new SeanceDAO(em);
		ResponseBuilder builder;
		builder = Response.ok(seanceDAO.getSeanceById(id));
		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}
	
	@OPTIONS
	@Path("/getSeance/{id}")
	public Response getSeanceOptions(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String sayPlainTextHello() {
		return "Hello Jersey seance !!";
	}

	/**
	 * Method that creates seance 
	 * @param user
	 * @return
	 */
	@POST @Consumes({ MediaType.APPLICATION_JSON })
	@Path("/createSeance")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response register(Seance seance) {

		SeanceDAO seanceDAO = new SeanceDAO(em);
		Boolean exec = seanceDAO.create(seance);

		ResponseBuilder builder;
		if(exec){
			builder = Response.ok(seance);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/createSeance")
	public Response getOptionsCreate(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}
	
	/**
	 * Method that modifies seance info
	 * @param user
	 * @return
	 */
	
	@POST @Consumes({ MediaType.APPLICATION_JSON })
	@Path("/updateSeance")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateSeance(Seance seance) {

		SeanceDAO seanceDAO = new SeanceDAO(em);
		Boolean exec = seanceDAO.update(seance);

		ResponseBuilder builder;
		if(exec){
			builder = Response.ok(seance);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/updateSeance")
	public Response getOptionsUpdate(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}


	/**
	 * method that deletes a seance
	 * @param id
	 * @return
	 */
	@DELETE
	@Path("/delete/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteById(@PathParam("id") int id) {
		SeanceDAO seanceDAO = new SeanceDAO(em);

		Boolean bool = seanceDAO.delete(id);

		ResponseBuilder builder;
		if(bool){
			builder = Response.ok(id);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}


		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();

	}
	@OPTIONS
	@Path("/delete/{id}")
	public Response getOptionsDeleteUser(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

}

