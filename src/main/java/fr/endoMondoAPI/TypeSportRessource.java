package fr.endoMondoAPI;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import fr.endoMondoDAO.MeteoDAO;
import fr.endoMondoDAO.SeanceDAO;
import fr.endoMondoDAO.TypeSportDAO;
import fr.endomondoEntity.Meteo;
import fr.endomondoEntity.Seance;
import fr.endomondoEntity.TypeSport;


@Path("/typeSport")
public class TypeSportRessource
{
	private EntityManagerFactory emf;
	private EntityManager em;

	public TypeSportRessource() {
		// Use persistence.xml configuration
		emf = Persistence.createEntityManagerFactory("endoMondoUpDate");
		// Retrieve an entity manager
		em = emf.createEntityManager();
	}

	/**
	 * Method that returns user
	 * @param id
	 * @return
	 */
	@GET
	@Path("/getAll")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAll() {
		TypeSportDAO typeSportDAO = new TypeSportDAO(em);
		List<TypeSport> listsport = typeSportDAO.getAll();

		ResponseBuilder builder = Response.ok(listsport);

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}
	
	@OPTIONS
	@Path("getAll")
	public Response getAllOptions(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}
	
	

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String sayPlainTextHello() {
		return "Hello Jersey typeSport !!";
	}

	/**
	 * Method that creates seance 
	 * @param user
	 * @return
	 */
	@POST @Consumes({ MediaType.APPLICATION_JSON })
	@Path("/createTypeSport")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response register(TypeSport sport) {

		TypeSportDAO typeSportDAO = new TypeSportDAO(em);
		Boolean exec = typeSportDAO.create(sport);

		ResponseBuilder builder;
		if(exec){
			builder = Response.ok(sport);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/createTypeSport")
	public Response getOptionsCreate(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}
	
	/**
	 * Method that modifies seance info
	 * @param user
	 * @return
	 */
	
	@POST @Consumes({ MediaType.APPLICATION_JSON })
	@Path("/updateTypeSport")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateTypeSport(TypeSport sport) {

		TypeSportDAO typeSportDAO = new TypeSportDAO(em);
		Boolean exec = typeSportDAO.update(sport);

		ResponseBuilder builder;
		if(exec){
			builder = Response.ok(sport);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/updateTypeSport")
	public Response getOptionsUpdate(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}


	/**
	 * method that deletes a seance
	 * @param id
	 * @return
	 */
	@DELETE
	@Path("/delete/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteById(@PathParam("id") int id) {
	
		TypeSportDAO typeSportDAO = new TypeSportDAO(em);
		Boolean exec = typeSportDAO.delete(id);
		
		ResponseBuilder builder;
		if(exec){
			builder = Response.ok(id);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}


		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();

	}
	@OPTIONS
	@Path("/delete/{id}")
	public Response getOptionsDeleteUser(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

}

