package fr.endoMondoAPI;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;






import fr.endoMondoDAO.SeanceDAO;
import fr.endoMondoDAO.UtilisateurDAO;
import fr.endomondoEntity.Seance;
import fr.endomondoEntity.Utilisateur;

@Path("/users")
public class UtilisateurResource {

	private EntityManagerFactory emf;
	private EntityManager em;

	public UtilisateurResource() {
		// Use persistence.xml configuration
		emf = Persistence.createEntityManagerFactory("endoMondoUpDate");
		// Retrieve an entity manager
		em = emf.createEntityManager();
	}

	/**
	 * Method that returns user
	 * @param id
	 * @return
	 */
	@GET
	@Path("getUser/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getUser(@PathParam("id") int id) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO(em);
		Utilisateur utilisateur = utilisateurDAO.getUserById(id);

		ResponseBuilder builder = Response.ok(utilisateur);

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}
	
	@OPTIONS
	@Path("getUser/{id}")
	public Response getUserOptions(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}
	
	/**
	 * Method that returns user
	 * @param id
	 * @return
	 */
	@GET
	@Path("/getAll")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAll() {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO(em);
		List<Utilisateur> listUsers = utilisateurDAO.getAll();

		ResponseBuilder builder = Response.ok(listUsers);

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}
	
	@OPTIONS
	@Path("getAll")
	public Response getAllOptions(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String sayPlainTextHello() {
		return "Hello Jersey !!";
	}


	/**
	 * Method that creates user 
	 * @param user
	 * @return
	 */
	@POST @Consumes({ MediaType.APPLICATION_JSON })
	@Path("/register")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response register(Utilisateur user) {

		UtilisateurDAO userDAO = new UtilisateurDAO(em);
		Boolean exec = userDAO.create(user);

		ResponseBuilder builder;
		if(exec){
			builder = Response.ok(user);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/register")
	public Response getOptionsCreate(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

	/**
	 * Method that modifies user info
	 * @param user
	 * @return
	 */
	@POST @Consumes({ MediaType.APPLICATION_JSON })
	@Path("/updateUser")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response UpdateUser(Utilisateur user) {

		UtilisateurDAO userDAO = new UtilisateurDAO(em);
//		Utilisateur userBase = userDAO.getUserById(user.getId());
		Boolean exec = userDAO.update(user);

		ResponseBuilder builder;
		if(exec){
			builder = Response.ok(user);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/updateUser")
	public Response getOptionsUpdate(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}


	
	/**
	 * Method that permits identification
	 * @param id
	 * @param email
	 * @param mdp
	 * @return true or false
	 */
	@POST @Consumes({ MediaType.APPLICATION_JSON })
	@Path("/identification")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response identificationUser(Utilisateur user){
		UtilisateurDAO userDAO = new UtilisateurDAO(em);

		Utilisateur userBase = userDAO.identificationUSer(user.getAdresseMail(), user.getMdp());
		//System.out.println(id);
		ResponseBuilder builder;
		if(userBase.getId()==0){
			builder = Response.status(Response.Status.BAD_REQUEST);
		}else{
			builder = Response.ok(userBase);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/identification")
	public Response getOptionsIdentification(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

	/**
	 * Method that returns the user listSeance
	 * @param id
	 * @return
	 */
	@GET
	@Path("/getSeance/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getSeance(@PathParam("id") int id) {
		UtilisateurDAO userDAO = new UtilisateurDAO(em);
		List<Seance> listSeance = userDAO.getSeance(id);

		ResponseBuilder builder;
		
		if(listSeance==null){
			builder = Response.status(Response.Status.BAD_REQUEST);
		}else{
			builder = Response.ok(listSeance);
		}

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();
	}

	@OPTIONS
	@Path("/getSeance/{id}")
	public Response getOptionsGetSeances(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

	/**
	 * Method that adds a seance to user listSeance
	 * @param id
	 */
	@PUT
	@Path("/addSeance/{id}&{idSeance}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response addSeance(@PathParam("id") int id, @PathParam("idSeance") int idSeance) {
		SeanceDAO seanceDAO = new SeanceDAO(em);
		Seance seance = seanceDAO.getSeanceById(idSeance);
		UtilisateurDAO userDAO = new UtilisateurDAO(em);
		List<Seance> listSeance = userDAO.getSeance(id);
		Boolean bool = listSeance.add(seance);

		ResponseBuilder builder;
		if(bool){
			builder = Response.ok(listSeance);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}


		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();

	}

	@OPTIONS
	@Path("/addSeance/{id}&{idSeance}")
	public Response getOptionsAddSeances(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

	/**
	 * Method that deletes a seance from user listSeance
	 * @param id
	 */
	@PUT
	@Path("deleteSeance/{id}&{idSeance}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteSeance(@PathParam("id") int id, @PathParam("idSeance") int idSeance) {
		SeanceDAO seanceDAO = new SeanceDAO(em);
		Seance seance = seanceDAO.getSeanceById(idSeance);
		UtilisateurDAO userDAO = new UtilisateurDAO(em);
		List<Seance> listSeance = userDAO.getSeance(id);
		Boolean bool = listSeance.remove(seance);

		ResponseBuilder builder;
		if(bool){
			builder = Response.ok(listSeance);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}


		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();

	}

	@OPTIONS
	@Path("deleteSeance/{id}&{idSeance}")
	public Response getOptionsDeleteSeances(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

	/**
	 * method that deletes a user
	 * @param id
	 * @return
	 */
	@DELETE
	@Path("/delete/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteById(@PathParam("id") int id) {
		UtilisateurDAO userDAO = new UtilisateurDAO(em);

		Boolean bool = userDAO.delete(id);

		ResponseBuilder builder;
		if(bool){
			builder = Response.ok(id);
		}else{
			builder = Response.status(Response.Status.BAD_REQUEST);
		}


		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");

		return builder.build();

	}
	@OPTIONS
	@Path("/delete/{id}")
	public Response getOptionsDeleteUser(){
		return Response.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

}
