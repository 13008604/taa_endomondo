package fr.endoMondoDAO;

import java.util.List;

public interface DAO<T> {
	public List<T> getAll();
	public boolean create(T obj);
	public boolean update(T obj);
	public boolean delete(int id);
	
}
