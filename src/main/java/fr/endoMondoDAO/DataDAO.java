package fr.endoMondoDAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;

public abstract class DataDAO {

        protected EntityManager em;
        protected EntityTransaction tx;
        protected CriteriaBuilder cb;

        public DataDAO(EntityManager em) {
                this.em = em;
                this.cb = em.getCriteriaBuilder();
        }
}