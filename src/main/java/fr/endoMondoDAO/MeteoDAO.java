package fr.endoMondoDAO;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fr.endomondoEntity.Meteo;
import fr.endomondoEntity.TypeSport;
import fr.endomondoEntity.Utilisateur;

public class MeteoDAO extends DataDAO implements DAO<Meteo>
{
	public MeteoDAO(EntityManager em) {
		super(em);
	}

	@Override
	public List<Meteo> getAll() {
		CriteriaQuery<Meteo> q = cb.createQuery(Meteo.class);
		Root<Meteo> c = q.from(Meteo.class);
		q.select(c);
		TypedQuery<Meteo> query = em.createQuery(q);
		return query.getResultList();
	}

	@Override
	public boolean create(Meteo obj) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.persist(obj);
			tx.commit();
			tx = null;
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Meteo obj) {
		tx = null;
		try {

			tx = em.getTransaction();
			tx.begin();
			em.merge(obj);
			tx.commit();
			tx = null;

		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

	@Override
	public boolean delete(int id) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			Meteo obj = getMeteoById(id);
			if (obj == null){
				return false;
			}else{
				em.remove(obj);
				tx.commit();
				tx = null;
			}
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}
		return true;

	}

	private Meteo getMeteoById(int id) {
		CriteriaQuery<Meteo> q = cb.createQuery(Meteo.class);
		Root<Meteo> c = q.from(Meteo.class);
		q.select(c).where(cb.equal(c.get("id"), id));
		
		TypedQuery<Meteo> query = em.createQuery(q);
		return query.getSingleResult();
	}

}

