package fr.endoMondoDAO;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fr.endomondoEntity.Parcours;
import fr.endomondoEntity.PointGPS;
import fr.endomondoEntity.Seance;
import fr.endomondoEntity.Utilisateur;


public class ParcoursDAO extends DataDAO implements DAO<Parcours>
{
	public ParcoursDAO(EntityManager em) {
		super(em);
	}

	@Override
	public List<Parcours> getAll() {
	
//		try {
//
//			String queryString = "SELECT parc FROM Parcours parc";
//			Query query = em.createQuery(queryString);
//			// get the list of all the users
//			List<Parcours> results = query.getResultList();
//
//			return results;
//
//		} catch (RuntimeException e) {
//			System.out.println(e.toString());
//		}
//		return null;
		
		CriteriaQuery<Parcours> q = cb.createQuery(Parcours.class);
		Root<Parcours> c = q.from(Parcours.class);
		q.select(c);
		TypedQuery<Parcours> query = em.createQuery(q);
		return query.getResultList();
	}

	public Parcours getParcoursById(int id) {

//		String queryString1 = "SELECT parc FROM Parcours parc where id=:idparam";
//		
//		try {
//			System.out.println("try");
//
//			Query query1 = em.createQuery(queryString1).setParameter("idparam", id);
//			Parcours parc = (Parcours) query1.getSingleResult();
//			return parc;
//
//		} catch (Exception e) {
//			System.out.println(e);
//		}
//
//		return null;
	
		CriteriaQuery<Parcours> q = cb.createQuery(Parcours.class);
		Root<Parcours> c = q.from(Parcours.class);
		q.select(c).where(cb.equal(c.get("id"), id));
		
		TypedQuery<Parcours> query = em.createQuery(q);
		return query.getSingleResult();
	}
	
	public List<PointGPS> getListPointsGPS(int id)
	{
//		String queryString1 = "SELECT points FROM PointGPS points where points.parcours.id=?1";
//	
//		try {
//			Query query1 = em.createQuery(queryString1).setParameter(1, id);
//			@SuppressWarnings("unchecked")
//			List<PointGPS> list = (List<PointGPS>) query1.getResultList();
//			return list;
//
//		} catch (Exception e) {
//			System.out.println(e);
//		}
//
//		return null;
		
		CriteriaQuery<PointGPS> q = cb.createQuery(PointGPS.class);
		Root<PointGPS> c = q.from(PointGPS.class);
		q.select(c).where(cb.equal(c.get("points.parcours.id"), id));
		
		TypedQuery<PointGPS> query = em.createQuery(q);
		return query.getResultList();
		
	}
	

	@Override
	public boolean create(Parcours obj) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.persist(obj);
			tx.commit();
			tx = null;

		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

	@Override
	public boolean update(Parcours obj) {
		tx = null;
		try {

			tx = em.getTransaction();
			tx.begin();
			em.merge(obj);
			tx.commit();
			tx = null;

		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

	@Override
	public boolean delete(int id) {
		tx = null;
		try {

			tx = em.getTransaction();
			tx.begin();
			Parcours obj = getParcoursById(id);
			if (obj == null){
				return false;
			}else{
				em.remove(obj);
				tx.commit();
				tx = null;
			}
				
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

}

