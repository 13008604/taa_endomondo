package fr.endoMondoDAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fr.endomondoEntity.Meteo;
import fr.endomondoEntity.Parcours;
import fr.endomondoEntity.PointGPS;
import fr.endomondoEntity.Seance;

public class PointGPSDAO extends DataDAO implements DAO<PointGPS>
{
	public PointGPSDAO(EntityManager em) {
		super(em);
	}

	@Override
	public List<PointGPS> getAll() {
//		try {
//
//			String queryString = "SELECT pt FROM PointGPS pt";
//			Query query = em.createQuery(queryString);
//			// get the list of all the users
//			List<PointGPS> results = query.getResultList();
//
//			return results;
//
//		} catch (RuntimeException e) {
//			System.out.println(e.toString());
//		}
//		return null;
		
		CriteriaQuery<PointGPS> q = cb.createQuery(PointGPS.class);
		Root<PointGPS> c = q.from(PointGPS.class);
		q.select(c);
		TypedQuery<PointGPS> query = em.createQuery(q);
		return query.getResultList();
	}
	
	public PointGPS getPointGPSById(int id) {
		
		CriteriaQuery<PointGPS> q = cb.createQuery(PointGPS.class);
		Root<PointGPS> c = q.from(PointGPS.class);
		q.select(c).where(cb.equal(c.get("id"), id));
		
		TypedQuery<PointGPS> query = em.createQuery(q);
		return query.getSingleResult();
	}
	
	

	@Override
	public boolean create(PointGPS obj) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.persist(obj);
			tx.commit();
			tx = null;

		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

	@Override
	public boolean update(PointGPS obj) {
		tx = null;
		try {

			tx = em.getTransaction();
			tx.begin();
			em.merge(obj);
			tx.commit();
			tx = null;

		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

	@Override
	public boolean delete(int id) {
		tx = null;
		try {

			tx = em.getTransaction();
			tx.begin();
			PointGPS obj = getPointGPSById(id);
			if (obj == null){
				return false;
			}else{
				em.remove(obj);
				tx.commit();
				tx = null;
			}
				
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

}

