package fr.endoMondoDAO;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import fr.endomondoEntity.Seance;

public class SeanceDAO extends DataDAO implements DAO<Seance>
{
	public SeanceDAO(EntityManager em) {
		super(em);
	}

	/**
	 * Get all seances
	 */
	@Override
	public List<Seance> getAll() {
		CriteriaQuery<Seance> q = cb.createQuery(Seance.class);
		Root<Seance> c = q.from(Seance.class);
		q.select(c);
		TypedQuery<Seance> query = em.createQuery(q);
		return query.getResultList();
	}
	
	/**
	 * Get one seance
	 * @param idSeance
	 * @return
	 */
	public Seance getSeanceById(int idSeance) {
		CriteriaQuery<Seance> q = cb.createQuery(Seance.class);
		Root<Seance> c = q.from(Seance.class);
		q.select(c).where(cb.equal(c.get("id"), idSeance));
		
		TypedQuery<Seance> query = em.createQuery(q);
		return query.getSingleResult();
	}


	@Override
	public boolean create(Seance obj) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.persist(obj);
			tx.commit();
			tx = null;
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

	@Override
	public boolean update(Seance obj) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.merge(obj);
			tx.commit();
			tx = null;
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

	@Override
	public boolean delete(int id) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			Seance obj = getSeanceById(id);
			if (obj == null)
				return false;
			System.out.println("idUser:"+id);
			em.remove(obj);
			tx.commit();
			tx = null;
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}
}