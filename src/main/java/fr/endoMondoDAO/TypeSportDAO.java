package fr.endoMondoDAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fr.endomondoEntity.Meteo;
import fr.endomondoEntity.TypeSport;
import fr.endomondoEntity.Utilisateur;

public class TypeSportDAO extends DataDAO implements DAO<TypeSport>
{
	public TypeSportDAO(EntityManager em) {
		super(em);
	}

	@Override
	public List<TypeSport> getAll() {
			CriteriaQuery<TypeSport> q = cb.createQuery(TypeSport.class);
			Root<TypeSport> c = q.from(TypeSport.class);
			q.select(c);
			TypedQuery<TypeSport> query = em.createQuery(q);
			return query.getResultList();
		}


	@Override
	public boolean create(TypeSport obj) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.persist(obj);
			tx.commit();
			tx = null;
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

	@Override
	public boolean update(TypeSport obj) {
		tx = null;
		try {

			tx = em.getTransaction();
			tx.begin();
			em.merge(obj);
			tx.commit();
			tx = null;

		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

	@Override
	public boolean delete(int id) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			TypeSport obj = getTypeSportById(id);
			if (obj == null){
				return false;
			}else{
				em.remove(obj);
				tx.commit();
				tx = null;
			}
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}
		return true;

	}

	private TypeSport getTypeSportById(int id) {
		CriteriaQuery<TypeSport> q = cb.createQuery(TypeSport.class);
		Root<TypeSport> c = q.from(TypeSport.class);
		q.select(c).where(cb.equal(c.get("id"), id));
		
		TypedQuery<TypeSport> query = em.createQuery(q);
		return query.getSingleResult();
	}

}
