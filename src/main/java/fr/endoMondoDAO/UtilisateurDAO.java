package fr.endoMondoDAO;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import fr.endomondoEntity.Seance;
import fr.endomondoEntity.Utilisateur;


public class UtilisateurDAO extends DataDAO implements DAO<Utilisateur>
{
	public UtilisateurDAO(EntityManager em) {
		super(em);
	}

	@Override
	public List<Utilisateur> getAll() {
		CriteriaQuery<Utilisateur> q = cb.createQuery(Utilisateur.class);
		Root<Utilisateur> c = q.from(Utilisateur.class);
		q.select(c);
		TypedQuery<Utilisateur> query = em.createQuery(q);
		return query.getResultList();
	}

	public Utilisateur getUserById(int id){
		CriteriaQuery<Utilisateur> q = cb.createQuery(Utilisateur.class);
		Root<Utilisateur> c = q.from(Utilisateur.class);
		q.select(c).where(cb.equal(c.get("id"), id));
		
		TypedQuery<Utilisateur> query = em.createQuery(q);
		return query.getSingleResult();
	}
	
	public Utilisateur getUserByMail(String adresseMail) {
		CriteriaQuery<Utilisateur> q = cb.createQuery(Utilisateur.class);
		Root<Utilisateur> c = q.from(Utilisateur.class);
		q.select(c).where(cb.equal(c.get("adresseMail"), adresseMail));
		
		TypedQuery<Utilisateur> query = em.createQuery(q);
		return query.getSingleResult();
	}

	@Override
	public boolean create(Utilisateur obj) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.persist(obj);
			tx.commit();
			tx = null;
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Utilisateur obj) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.merge(obj);
			tx.commit();
			tx = null;
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(int id) {
		tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			Utilisateur obj = getUserById(id);
			if (obj == null){
				return false;
			}else{
				em.remove(obj);
				tx.commit();
				tx = null;
			}
		} catch (RuntimeException e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}
	
	public Utilisateur identificationUSer(String adresseMail, String mdp) {
		CriteriaQuery<Utilisateur> q = cb.createQuery(Utilisateur.class);
		Root<Utilisateur> c = q.from(Utilisateur.class);
		q.select(c).where(
					cb.equal(c.get("adresseMail"), adresseMail),
					cb.equal(c.get("mdp"), mdp)
		);
		
		TypedQuery<Utilisateur> query = em.createQuery(q);
		System.out.println("identification user !");
		return query.getSingleResult();
	}

	public List<Seance> getSeance(int userId) {
		CriteriaQuery<Seance> q = cb.createQuery(Seance.class);
		Root<Seance> c = q.from(Seance.class);
		q.select(c);
		q.select(c).where(cb.equal(c.get("utilisateur"), userId));
		
		TypedQuery<Seance> query = em.createQuery(q);
		return query.getResultList();
	}
}