package fr.endomondoEntity;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.GeneratedValue;



@javax.persistence.Entity 
public class Cardio
{
	
	@javax.persistence.Id 
	@javax.persistence.Column(nullable = false) 
	@GeneratedValue
	protected int id = 0;
	
	@javax.persistence.Column(nullable = false) 
	protected double frequence;


	public Cardio(){
		super();
	}

	public double getFrequence() {
		return this.frequence;	
	}
	

	public int getId() {
		return this.id;	
	}
	
	public void setId(int id) {
		this.id=id;	
	}

	public void setFrequence(double myFrequence) {
		this.frequence = myFrequence;	
	}
}

