package fr.endomondoEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Use persistence.xml configuration
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("manager1");
		// Retrieve an entity manager
		EntityManager em = emf.createEntityManager();
		
		
		EntityTransaction tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			// Your entity becomes persistant
			System.out.println("toto");
			Utilisateur user = new Utilisateur();
			user.setNom("toto");
			user.setAdresseMail("a");
			user.setAvatar("a");
			user.setCompteFB("a");
			user.setCompteTwitter("a");
			user.setDateNaissance("a");
			user.setPoids(10.0);
			user.setPrenom("a");
//			user.setSexe("a");
			user.setMdp("");
			user.setTaille(10);
			
			em.persist(user);
			tx.commit(); //do the flush automatically
		} catch (RuntimeException e) {
			if (tx != null && tx.isActive())
				tx.rollback();
			throw e; // or display error message
		} finally {
			em.close();
		}
		emf.close(); // close at application end
		
	}

}
