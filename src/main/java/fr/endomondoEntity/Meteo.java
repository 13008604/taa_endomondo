package fr.endomondoEntity;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.GeneratedValue;


 
@javax.persistence.Entity 
public class Meteo
{

	
	@javax.persistence.Id 
	@javax.persistence.Column(nullable = false) 
	@GeneratedValue
	protected int id = 0;

	
	@javax.persistence.Column(nullable = false) 
	protected String nomMeteo;
	

	public Meteo(){
		super();
	}

	
	public int getId() {
		return this.id;	
	}
	
	public void setId(int id) {
		this.id=id;	
	}
	
	public String getNomMeteo() {
		return nomMeteo;
	}

	public void setNomMeteo(String nomMeteo) {
		this.nomMeteo = nomMeteo;
	}
	
	
}

