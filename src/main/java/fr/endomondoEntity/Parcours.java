package fr.endomondoEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonManagedReference;


 
@javax.persistence.Entity 
public class Parcours
{
	 
	@javax.persistence.Id 
	@javax.persistence.Column(nullable = false) 
	@GeneratedValue
	protected int id = 0;

 
	@javax.persistence.Column(nullable = false) 
	protected String depart;
 
	 
	@javax.persistence.Column(nullable = false) 
	protected String arrivee;
 
	 
	@OneToMany(fetch = FetchType.EAGER,cascade=CascadeType.MERGE, mappedBy = "parcours")
	protected List<PointGPS> pointGPS;

 
//	@javax.persistence.OneToOne(cascade = CascadeType.REMOVE, mappedBy = "parcours")
//	protected List<Seance> seance;


	//Constructor
	public Parcours(){
		super();
	}
	public Parcours(int id){
		super();
		this.id=id;
	}

	
	//Getters and setters

	@JsonManagedReference
	public List<PointGPS> getPointGPS() {
		if(this.pointGPS == null) {
			this.pointGPS = new ArrayList<PointGPS>();
		}
		return (List<PointGPS>) this.pointGPS;	
	}


	public void setPointGPS(List<PointGPS> pointGPS) {
		this.pointGPS = pointGPS;
	}

	public void addPointGPS(PointGPS newPointGPS) {
	    //prevent endless loop
	    if (pointGPS.contains(newPointGPS))
	      return ;
	    //add new account
	    pointGPS.add(newPointGPS);
	    //set myself into the twitter account
	    newPointGPS.setParcours(this);
	  }
	  
	
	  public void removePointGPS(PointGPS oldPointGPS) {
	    //prevent endless loop
	    if (!pointGPS.contains(oldPointGPS))
	      return ;
	    //remove the account
	    pointGPS.remove(oldPointGPS);
	    //remove myself from the twitter account
	    oldPointGPS.setParcours(null);
	  }

//	public List<Seance> getSeance() {
//		return seance;
//	}
//
//
//	public void setSeance(List<Seance> seance) {
//		this.seance = seance;
//	}

	 
	public String getDepart() {
		return this.depart;	
	}
	
 
	public String getArrivee() {
		return this.arrivee;	
	}
	
	 
//	public Set<PointGPS> getPointGPS() {
//		if(this.pointGPS == null) {
//				this.pointGPS = new HashSet<PointGPS>();
//		}
//		return (Set<PointGPS>) this.pointGPS;	
//	}
	
	
	 
	public int getId() {
		return this.id;	
	}
	
 
	public void setDepart(String myDepart) {
		this.depart = myDepart;	
	}
	
	 
	public void setArrivee(String myArrivee) {
		this.arrivee = myArrivee;	
	}


	public void setId(int id_parcours) {
		this.id = id_parcours;
	}
	
}

