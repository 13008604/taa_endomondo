package fr.endomondoEntity;

import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;


@javax.persistence.Entity 
public class PointGPS
{

	@javax.persistence.Id 
	@javax.persistence.Column(nullable = false) 
	@GeneratedValue
	protected int id = 0;

	@javax.persistence.Column(nullable = false) 
	protected double x;

		 
	@javax.persistence.Column(nullable = false) 
	protected double y;

	@javax.persistence.Column(nullable = false) 
	protected double z;

	 
	@javax.persistence.OneToOne 
	protected Cardio cardio;

	
	@javax.persistence.ManyToOne 
	@JoinColumn(name="parcours_id")
	protected Parcours parcours;

	//Constructor
	public PointGPS(){
		super();
	}

	//Getters and setters
	public Parcours getParcours() {
		return parcours;
	}

	public void setParcours(Parcours newParcours) {
		  //prevent endless loop
		    if (sameAsParcours(newParcours))
		      return ;
		    //set new user
		    Parcours oldParcours = this.parcours;
		    this.parcours = newParcours;
		    //remove from the old seance
		    if (oldParcours!=null && oldParcours.getPointGPS().size()!=0)
		    	oldParcours.removePointGPS(this);
		    //set myself into new seance
		    if (oldParcours!=null)
		    	parcours.addPointGPS(this);
	  }
		  

	public void setId(int id) {
		this.id = id;
	}

	
	public double getX() {
		return this.x;	
	}
	
	
	public double getY() {
		return this.y;	
	}
	

	public double getZ() {
		return this.z;	
	}
	
	public Cardio getCardio() {
		return this.cardio;	
	}
	
	public int getId() {
		return this.id;	
	}
	
	public void setX(double myX) {
		this.x = myX;	
	}
	
	
	public void setY(double myY) {
		this.y = myY;	
	}
	
	
	public void setZ(double myZ) {
		this.z = myZ;	
	}
	
	
	public void setCardio(Cardio myCardio) {
		this.cardio=myCardio;	
	}
	
	//Method that prevent endless loop
	 private boolean sameAsParcours(Parcours newParcours) {
		    return parcours==null? newParcours == null : parcours.equals(newParcours);
		  }

	
}

