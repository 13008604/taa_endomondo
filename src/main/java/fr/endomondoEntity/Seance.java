package fr.endomondoEntity;
import java.sql.Time;
import java.util.Date;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hsqldb.types.DateTimeType;


 
@javax.persistence.Entity 
public class Seance
{
	
	@javax.persistence.Id 
	@javax.persistence.Column(nullable = false) 
	@GeneratedValue
	protected int id = 0;

		 
	@javax.persistence.Column(nullable = true) 
	protected String duree;
	 
	@javax.persistence.Temporal(javax.persistence.TemporalType.DATE) 
	@javax.persistence.Column(nullable = false) 
	protected Date date;
	 
	@javax.persistence.Column(nullable = false) 
	protected Time heureDepart;

	@javax.persistence.Column(nullable = true) 
	protected Time heureFin;

	@javax.persistence.Column(nullable = false) 
	protected double distance;

	 
	@javax.persistence.Column(nullable = false) 
	protected double vitesse;

	@javax.persistence.Column(nullable = true, columnDefinition="Decimal(10,2) default '0,0'") 
	protected double calories;

	@javax.persistence.OneToOne()
	//@JoinColumn(name="id_typeSport", referencedColumnName="seance_typeSport")
	protected TypeSport typeSport;

	@javax.persistence.OneToOne()
	//@javax.persistence.JoinColumn(nullable = true) 
	protected Meteo meteo;
	
	 
	@javax.persistence.OneToOne()
	//@javax.persistence.JoinColumn(nullable = true) 
	protected Parcours parcours;
	 
	@javax.persistence.ManyToOne 
	@JoinColumn(name="utilisateur_id")
	protected Utilisateur utilisateur;

	public Seance(){
		super();
	}


	public String getDuree() {
		return this.duree;	
	}

	public Date getDate() {
		return this.date;	
	}
	
	public Time getHeureDepart() {
		return this.heureDepart;	
	}
	
	public Time getHeureFin() {
		return this.heureFin;	
	}
	
	public double getDistance() {
		return this.distance;	
	}
	
	public double getVitesse() {
		return this.vitesse;	
	}
	
	
	public double getCalories() {
		return this.calories;	
	}
	
	public TypeSport getTypeSport() {
		return this.typeSport;	
	}
	
	public Meteo getMeteo() {
		return this.meteo;	
	}
	
	
	public Parcours getParcours() {
		return this.parcours;	
	}
	
	 @JsonBackReference
	public Utilisateur getUtilisateur() {
		return this.utilisateur;	
	}
	
	public int getId() {
		return this.id;	
	}
	
	public void setDuree(String myDuree) {
		this.duree = myDuree;	
	}
	
	public void setDate(Date myDate) {
		this.date = myDate;	
	}
	
	
	public void setHeureDepart(Time myHeureDepart) {
		this.heureDepart = myHeureDepart;	
	}
	
	
	public void setHeureFin(Time myHeureFin) {
		this.heureFin = myHeureFin;	
	}
	
	public void setDistance(double myDistance) {
		this.distance = myDistance;	
	}
	
	public void setVitesse(double myVitesse) {
		this.vitesse = myVitesse;	
	}
	
	public void setCalories(double myCalories) {
		this.calories = myCalories;	
	}
	

	public void setId(int id_seance) {
		this.id = id_seance;
	}


	public void setTypeSport(TypeSport typeSport) {
		this.typeSport = typeSport;
	}


	public void setMeteo(Meteo meteo) {
		this.meteo = meteo;
	}


	public void setParcours(Parcours parcours) {
		this.parcours = parcours;
	}
	
	  public void setUtilisateur(Utilisateur newUtilisateur) {
		  //prevent endless loop
		    if (sameAsUtilisateur(newUtilisateur))
		      return ;
		    //set new user
		    Utilisateur olduser = this.utilisateur;
		    this.utilisateur = newUtilisateur;
		    //remove from the old seance
		    if (olduser!=null && olduser.getSeances().size()!=0)
		    	olduser.removeSeance(this);
		    //set myself into new seance
		    if (olduser!=null)
		    	utilisateur.addSeance(this);
	  }
		  
	  private boolean sameAsUtilisateur(Utilisateur newUser) {
		    return utilisateur==null? newUser == null : utilisateur.equals(newUser);
		  }
}

