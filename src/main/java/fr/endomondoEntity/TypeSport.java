package fr.endomondoEntity;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToOne;


 
@javax.persistence.Entity 
public class TypeSport
{
	
	//@OneToOne(mappedBy="typeSport")
	@javax.persistence.Id 
	@javax.persistence.Column(nullable = false) 
	@GeneratedValue
	protected int id = 0;
	
	@javax.persistence.Column(nullable = false) 
	protected String nomSport;


	public TypeSport(){
		super();
	}

	
	public String getNomSport() {
		return this.nomSport;	
	}
	


	public void setId(int id_typeSport) {
		this.id = id_typeSport;
	}


	public int getId() {
		return this.id;	
	}
	
	
	public void setNomSport(String myNomSport) {
		this.nomSport = myNomSport;	
	}
	
	
	public void unsetNomSport() {
		this.nomSport = "";	
	}
	
	
}

