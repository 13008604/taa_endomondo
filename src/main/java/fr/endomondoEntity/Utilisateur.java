package fr.endomondoEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonManagedReference;



@JsonIgnoreProperties
@javax.persistence.Entity 
public class Utilisateur
{
	
	@javax.persistence.Id 
	@javax.persistence.Column(nullable = false) 
	@GeneratedValue
	protected int id = 0;

	
	@javax.persistence.Column(nullable = false) 
	protected String nom;


	@javax.persistence.Column(nullable = false) 
	protected String prenom;

	 
	@javax.persistence.Column(nullable = true) 
	protected String dateNaissance;

	
	@javax.persistence.Column(nullable = true, columnDefinition="Decimal(10,2) default '0,0'") 
	protected double poids;

	
	 
	@javax.persistence.Column(nullable = true, columnDefinition="Decimal(10,2) default '0,0'") 
	protected double taille;

	
	 
	@javax.persistence.Column(nullable = true) 
	protected boolean sexe;

	
	@javax.persistence.Column(nullable = true) 
	protected String compteFB;

	
	 
	@javax.persistence.Column(nullable = true) 
	protected String compteTwitter;

	
	 
	@javax.persistence.Column(nullable = false, unique=true) 
	protected String adresseMail;

	
	 
	@javax.persistence.Column(nullable = true) 
	protected String avatar;


	 
	@javax.persistence.Column(nullable = false) 
	protected String mdp;

	 
	@OneToMany(fetch = FetchType.EAGER,cascade=CascadeType.MERGE, mappedBy = "utilisateur")
	protected List<Seance> seances;

	
	 
	@javax.persistence.OneToMany() 
	protected List<Utilisateur> amis;

	
//	@javax.persistence.ManyToOne 
//	protected Utilisateur utilisateur;
	

	public void setSeance(List<Seance> seances) {
		this.seances = seances;
	}

	public Utilisateur(){
		super();
	}
	
	public Utilisateur(int id){
		super();
		this.id=id;
	}

	public String getNom() {
		return this.nom;	
	}
	
	
	public String getPrenom() {
		return this.prenom;	
	}
	

	public String getDateNaissance() {
		return this.dateNaissance;	
	}
	
	
	public double getPoids() {
		return this.poids;	
	}
	
	public double getTaille() {
		return this.taille;	
	}
	

	public boolean getSexe() {
		return this.sexe;	
	}
	
	
	public String getCompteFB() {
		return this.compteFB;	
	}
	
	public String getCompteTwitter() {
		return this.compteTwitter;	
	}

	public String getAdresseMail() {
		return this.adresseMail;	
	}
	

	public String getAvatar() {
		return this.avatar;	
	}
	
	
	public String getMdp() {
		return this.mdp;	
	}
	
    @JsonManagedReference
	public List<Seance> getSeances() {
		if(this.seances == null) {
			this.seances = new ArrayList<Seance>();
		}
		return (List<Seance>) this.seances;	
	}
	

	public List<Utilisateur> getAmis() {
		if(this.amis == null) {
				this.amis = new ArrayList<Utilisateur>();
		}
		return (List<Utilisateur>) this.amis;	
	}

	
	
	public int getId() {
		return this.id;	
	}
	
	public void setId(int id){
		this.id=id;
	}


	public void setAmis(List<Utilisateur> amis) {
		this.amis = amis;
	}

	public void setNom(String myNom) {
		this.nom = myNom;	
	}
	
	
	public void setPrenom(String myPrenom) {
		this.prenom = myPrenom;	
	}
	
	
	public void setDateNaissance(String myDateNaissance) {
		this.dateNaissance = myDateNaissance;	
	}
	
	
	public void setPoids(double myPoids) {
		this.poids = myPoids;	
	}
	
	
	public void setTaille(double myTaille) {
		this.taille = myTaille;	
	}
	
	
	public void setSexe(boolean mySexe) {
		this.sexe = mySexe;	
	}
	
	
	public void setCompteFB(String myCompteFB) {
		this.compteFB = myCompteFB;	
	}
	
	
	public void setCompteTwitter(String myCompteTwitter) {
		this.compteTwitter = myCompteTwitter;	
	}
	
	
	public void setAdresseMail(String myAdresseMail) {
		this.adresseMail = myAdresseMail;	
	}
	
	
	public void setAvatar(String myAvatar) {
		this.avatar = myAvatar;	
	}
	
	
	public void setMdp(String myMdp) {
		this.mdp = myMdp;	
	}
	
	public void addSeance(Seance seance) {
	    //prevent endless loop
	    if (seances.contains(seance))
	      return ;
	    //add new account
	    seances.add(seance);
	    //set myself into the twitter account
	    seance.setUtilisateur(this);
	  }
	  
	
	  public void removeSeance(Seance seance) {
	    //prevent endless loop
	    if (!seances.contains(seance))
	      return ;
	    //remove the account
	    seances.remove(seance);
	    //remove myself from the twitter account
	    seance.setUtilisateur(null);
	  }

}

