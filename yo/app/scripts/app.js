'use strict';

angular.module('yoApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap'
])
.config(function ($routeProvider) {
	$routeProvider
	  .when('/', {
		templateUrl: 'views/main.html',
		controller: 'MainCtrl'
	  })
	  .when('/login', {
		templateUrl: 'views/login.html',
		controller: 'LoginCtrl'
	  })
	  .when('/register', {
		templateUrl: 'views/register.html',
		controller: 'RegisterCtrl'
	  })
	  .when('/seance', {
		templateUrl: 'views/seance.html',
		controller: 'SeanceCtrl'
	  })
	  .when('/amis', {
		templateUrl: 'views/amis.html',
		controller: 'AmisCtrl'
	  })
	  .when('/createSeance', {
	    templateUrl: 'views/createSeance.html',
	    controller: 'CreateseanceCtrl'
	  })
	  .when('/profil', {
	    templateUrl: 'views/profil.html',
	    controller: 'ProfilCtrl'
	  })
	  .when('/updateProfil', {
	    templateUrl: 'views/updateProfil.html',
	    controller: 'UpdateprofilCtrl'
	  })
.when('/getSeance', {
  templateUrl: 'views/getSeance.html',
  controller: 'GetseanceCtrl'
})
	  .otherwise({
		redirectTo: '/'
	  });
})
.run(function($location, $cookieStore, $rootScope){
	if(!$cookieStore.get('id')){
		$location.path('/login');
	}
	$rootScope.capitaliseFirstLetter = function(string){
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	$rootScope.intField = function(number){
		if(parseInt(number) == 0){
			return "Non renseigné";
		}
		else{
			return number;
		}
	}
	$rootScope.stringField = function(str){
		if(str == null || str == ""){
			return "Non renseigné";
		}
		else{
			return str;
		}
	}
	$rootScope.emptyStr = function(str){
		if(str == null){
			return "";
		}
		else{
			return str;
		}
	}
	$rootScope.translateSexe = function(sexe){
		if(sexe == true){
			return "Femme";
		}
		else{
			return "Homme";
		}
	}
	
	$rootScope.displayError = function(msg){
		var html = '<div class="alert alert-danger alert-dismissable container"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Erreur !</strong> '+msg+'</div>';
		document.getElementById("message_box").innerHTML = html;
		setTimeout(dismissAlert, 3000);
	}
	$rootScope.displaySuccess = function(msg){
		var html = '<div class="alert alert-success alert-dismissable container"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+msg+'</div>';
		document.getElementById("message_box").innerHTML = html;
		setTimeout(dismissAlert, 3000);
	}
	function dismissAlert(){
		document.getElementById("message_box").innerHTML = "";
	}
	
});

