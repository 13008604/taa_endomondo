'use strict';

angular.module('yoApp')
  .controller('CreateseanceCtrl', function ($scope, $cookieStore, $http, $rootScope) {
	$scope.formData = {};
	$scope.formData.utilisateur = $cookieStore.get('id');
	
	$http({
		url: "http://localhost:8080/taa_endomondo/rest/meteo/getAll",
		method: "GET",
		headers: {
			"Content-Type" : "application/json"
		}
	}).success(function(data, status, headers, config) {
		console.log(data);
		$scope.listMeteo = data;
	}).error(function(data, status, headers, config) {
		$rootScope.displayError("Erreur");
	});
	
	
	$scope.submit = function(){
		this.formData.date = parseInt(this.formData.date);
		this.formData.distance = parseInt(this.formData.distance);
		this.formData.heureDepart = parseInt(this.formData.heureDepart);
		this.formData.vitesse = parseInt(this.formData.vitesse);
		console.log(this.formData);
		$http({
			url: "http://localhost:8080/taa_endomondo/rest/seances/createSeance",
			method: "POST",
			data: this.formData,
			headers: {
				"Content-Type" : "application/json"
			}
		}).success(function(data, status, headers, config) {
			$rootScope.displaySuccess("Séance bien ajoutée");
		}).error(function(data, status, headers, config) {
			$rootScope.displayError("Erreur lors de l'ajout d'une séance");
		});
	}
  });
