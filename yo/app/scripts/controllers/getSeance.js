'use strict';

angular.module('yoApp')
  .controller('GetseanceCtrl', function ($scope, $location, $http, $rootScope) {
    var id = parseInt($location.url().split("=")[1]);
	
	$http({
		url: "http://localhost:8080/taa_endomondo/rest/seances/getSeance/"+id,
		method: "GET",
		headers: {
			"Content-Type" : "application/json"	
		}
	}).success(function(data, status, headers, config) {
		console.log(data);
		$scope.id = id;
		$scope.date = data.date;
		$scope.heureDepart = data.heureDepart;
		$scope.distance = data.distance;
		$scope.vitesse = data.vitesse;
		$scope.heureFin = $rootScope.stringField(data.heureFin);
		$scope.parcours = $rootScope.stringField(data.parcours);
		$scope.duree = $rootScope.stringField(data.duree);
		$scope.meteo = $rootScope.stringField(data.meteo);
		$scope.calories = $rootScope.stringField(data.calories);
		$scope.typeSport = $rootScope.stringField(data.typeSport);
		
	}).error(function(data, status, headers, config) {
		$rootScope.displayError("Erreur");
	});
  });
