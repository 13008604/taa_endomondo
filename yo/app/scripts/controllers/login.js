'use strict';

angular.module('yoApp')
  .controller('LoginCtrl', function ($scope, $http, $location, $cookieStore, $rootScope) {
	if($cookieStore.get('id')){
		$cookieStore.remove('id');
	}
	$scope.formData = {};
    $scope.submit = function(){
		console.log(this.formData);
		$http({
			url: "http://localhost:8080/taa_endomondo/rest/users/identification",
			method: "POST",
			data: this.formData,
			headers: {
				"Content-Type" : "application/json"
			}
		}).success(function(data, status, headers, config) {
			$cookieStore.put('id', data.id);
			$cookieStore.put('prenom', data.prenom);
			$cookieStore.put('nom', data.nom);
			$cookieStore.put('mdp', data.mdp);
			$cookieStore.put('adresseMail', data.adresseMail);
			$location.path('/');
		}).error(function(data, status, headers, config) {
			$rootScope.displayError("Identifiant ou mot de passe incorrect !");
		});
	}
	$scope.register = function(){
		$location.path('/register');
	}
  });
