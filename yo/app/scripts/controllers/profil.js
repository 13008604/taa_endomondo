'use strict';

angular.module('yoApp')
  .controller('ProfilCtrl', function ($scope, $cookieStore, $http, $location, $rootScope) {
	var id = parseInt($cookieStore.get('id'));
	
	$http({
		url: "http://localhost:8080/taa_endomondo/rest/users/getUser/"+id,
		method: "GET",
		headers: {
			"Content-Type" : "application/json"
		}
	}).success(function(data, status, headers, config) {
		$scope.nom = $rootScope.capitaliseFirstLetter(data.nom);
		$scope.prenom = $rootScope.capitaliseFirstLetter(data.prenom);
		$scope.adresseMail = data.adresseMail;
		$scope.poids = $rootScope.intField(data.poids);
		$scope.taille = $rootScope.intField(data.taille/100);
		$scope.dateNaissance = $rootScope.stringField(data.dateNaissance);
		$scope.sexe = $rootScope.translateSexe(data.sexe);
		
		if(data.avatar != "" && data.avatar != null){
			document.getElementById("avatar").src = data.avatar;
		}	
	}).error(function(data, status, headers, config) {
		$rootScope.displayError("Erreur");
	});
  });
