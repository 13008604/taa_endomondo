'use strict';

angular.module('yoApp')
  .controller('RegisterCtrl', function ($scope, $http, $location, $cookieStore) {
	$scope.formData = {};
    $scope.submit = function(){
		console.log(this.formData);
		$http({
			url: "http://localhost:8080/taa_endomondo/rest/users/register",
			method: "POST",
			data: this.formData,
			headers: {
				"Content-Type" : "application/json"
			}
		}).success(function(data, status, headers, config) {
			alert("Inscription réussie !");
			$cookieStore.put('id', data.id);
			$cookieStore.put('prenom', data.prenom);
			$cookieStore.put('nom', data.nom);
			$location.path('/');
		}).error(function(data, status, headers, config) {
			alert("Erreur lors de l'inscription");
		});
	}
  });
