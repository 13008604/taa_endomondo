'use strict';

angular.module('yoApp')
  .controller('SeanceCtrl', function ($scope, $cookieStore, $http, $location, $rootScope){		
	var id = parseInt($cookieStore.get('id'));
	
	$http({
		url: "http://localhost:8080/taa_endomondo/rest/users/getUser/"+id,
		method: "GET",
		headers: {
			"Content-Type" : "application/json"
		}
	}).success(function(data, status, headers, config) {
		$scope.listSeances = data.seances;
	}).error(function(data, status, headers, config) {
		$rootScope.displayError("Erreur");
	});
  });
