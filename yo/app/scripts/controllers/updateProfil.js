'use strict';

angular.module('yoApp')
  .controller('UpdateprofilCtrl', function ($scope, $cookieStore, $http, $rootScope) {
	$scope.formData = {};
	//Init
	var id = parseInt($cookieStore.get('id'));
	$http({
		url: "http://localhost:8080/taa_endomondo/rest/users/getUser/"+id,
		method: "GET",
		headers: {
			"Content-Type" : "application/json"
		}
	}).success(function(data, status, headers, config) {
		$scope.formData.nom = $rootScope.capitaliseFirstLetter(data.nom);
		$scope.formData.prenom = $rootScope.capitaliseFirstLetter(data.prenom);
		$scope.formData.adresseMail = data.adresseMail;
		$scope.formData.avatar = data.avatar;
		$scope.formData.poids = data.poids;
		$scope.formData.taille = data.taille;
		$scope.formData.dateNaissance = $rootScope.emptyStr(data.dateNaissance);
		document.getElementById("sexe_"+data.sexe).selected = true;
	}).error(function(data, status, headers, config) {
		$rootScope.displayError("Erreur lors du chargement du profil");
	});
	
	//Submit
	$scope.formData.mdp = $cookieStore.get('mdp');
	$scope.formData.id = $cookieStore.get('id');
	
	$scope.submit = function(){
		console.log(this.formData);
		$http({
			url: "http://localhost:8080/taa_endomondo/rest/users/updateUser",
			method: "POST",
			data: this.formData,
			headers: {
				"Content-Type" : "application/json"
			}
		}).success(function(data, status, headers, config) {
			$cookieStore.put('id', data.id);
			$cookieStore.put('prenom', data.prenom);
			$cookieStore.put('nom', data.nom);
			$cookieStore.put('mdp', data.mdp);
			$cookieStore.put('adresseMail', data.adresseMail);
			$rootScope.displaySuccess("La modification de votre profil à bien était prise en compte.");
		}).error(function(data, status, headers, config) {
			$rootScope.displayError("Erreur lors de la modification du profil");
		});
	}
  });
